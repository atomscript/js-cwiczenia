var jsonData;

$.getJSON('http://atomscript.com/pl/json/index.php', function (json) {
    jsonData = json;
});

function pokazDane(typ) {
    var myData = jsonData;

    if (typ === 1) {
        myData = jsonData.firma;
    } else if (typ === 2) {
        myData = jsonData.indywidualny;
    }

    var jsonString = JSON.stringify(myData, null, 2);

    // document.getElementById('js-content').querySelector('pre').innerHTML = jsonString;
    $('#js-content pre').html(jsonString);
}