var jsonData;

(function () {
    wyczyscFormularz();
})();

function zaladujDane() {
    // $.getJSON('http://atomscript.com/pl/json/index.php', function () {
    // }).done(function (json) {
    //     jsonData = json;
    //     pokazKlientow();
    // });

    $.ajax({
        dataType: 'json',
        data: 'abcd',
        url: 'http://atomscript.com/pl/json/index.php',
        success: function () {
            // co zrobic jak otrzymamy sukces czyli status 200!!
        },
        error: function () {
            // co zrobić jak nie dostaniemy 200!!!
            alert('chyba mamy problem');
        }
    }).done(function (json) {
        jsonData = json;
        pokazKlientow();
    });
}

function pokazKlientow() {
    var jsonKeys = Object.keys(jsonData);
    for (i = 0; i < jsonKeys.length; i++) {
        var kContaier = document.createElement('div');
        kContaier.setAttribute('class', 'radio');

        var etykieta = document.createTextNode(jsonKeys[i]);

        var kLabel = document.createElement('label');

        var kInput = document.createElement('input');
        kInput.setAttribute('type', 'radio');
        kInput.setAttribute('name', 'klient');
        kInput.setAttribute('value', jsonKeys[i]);

        kLabel.appendChild(kInput);
        kLabel.appendChild(etykieta);

        kContaier.appendChild(kLabel);

        kContaier.querySelector('input').onchange = function () {
            pokazProdukty(this.getAttribute('value'));
            ukryjWyslaneDane();
        }

        document.getElementById('step-1').appendChild(kContaier);
    }
}

function pokazProdukty(typKlienta) {
    usunAlerty();
    wyczyscElement('step-2');
    wyczyscElement('step-3', true);

    if (typKlienta === 'firma' || typKlienta === 'ngo') {
        // pokaż NIP
        $('#step-1-nip').removeClass('hidden');
        $('#step-1-pesel').addClass('hidden');
        // document.getElementById('step-1-nip').removeAttribute('class', 'hidden');
    } else if (typKlienta === 'indywidualny') {
        // pokaż PESEL
        $('#step-1-nip').addClass('hidden');
        $('#step-1-pesel').removeClass('hidden');
    }

    var myData = jsonData[typKlienta];

    for (var i = 0; i < myData.length; i++) {
        var produkt = myData[i];
        var oprocentowanie = produkt.oprocentowanie * 100;
        var etykietaText = produkt.produkt + ' (' + oprocentowanie + '% w skali roku)';
        var additionalAttributes = [
            ['data-id', i],
            ['data-parent', typKlienta]
        ];

        // pContainer = generateRadioInput('produkt', produkt.produkt, etykietaText, additionalAttributes);
        var pContainer = document.createElement('div');
        pContainer.setAttribute('class', 'radio');

        var etykieta = document.createTextNode(etykietaText);
        var pInput = document.createElement('input');
        pInput.setAttribute('type', 'radio');
        pInput.setAttribute('name', 'produkt');
        pInput.setAttribute('value', produkt.produkt);

        pInput.setAttribute('data-id', i);
        pInput.setAttribute('data-parent', typKlienta);

        pLabel = document.createElement('label');
        pLabel.appendChild(pInput);
        pLabel.appendChild(etykieta);

        pContainer.appendChild(pLabel);

        // pInput.onchange = function () { pokazOkresy(this) };
        pContainer.querySelector('input').onchange = function () {
            pokazOkresy(this);
            ukryjWyslaneDane();
        };

        document.getElementById('step-2').appendChild(pContainer);
    }

    ukryjHelper('step-2');
}

function pokazOkresy(produkt) {
    wyczyscElement('step-3');
    usunAlerty();

    var produktId = produkt.getAttribute('data-id');
    var typKlienta = produkt.getAttribute('data-parent');

    var myData = jsonData[typKlienta][produktId].okresy;

    for (var i = 0; i < myData.length; i++) {
        // var okresInput = generateRadioInput('okres', myData[i], myData[i] + ' miesięcy');

        var pContainer = document.createElement('div');
        pContainer.setAttribute('class', 'radio');

        var etykieta = document.createTextNode(myData[i] + ' miesięcy');
        var pInput = document.createElement('input');
        pInput.setAttribute('type', 'radio');
        pInput.setAttribute('name', 'okres');
        pInput.setAttribute('value', myData[i]);

        pLabel = document.createElement('label');
        pLabel.appendChild(pInput);
        pLabel.appendChild(etykieta);

        pContainer.appendChild(pLabel);

        pInput.onchange = function () {
            ukryjWyslaneDane();
        }

        document.getElementById('step-3').appendChild(pContainer);
    }

    ukryjHelper('step-3');
}

function wyslijDane() {
    if (walidacja()) {
        var selectedRadios = document.querySelectorAll('input:checked');

        var outputData = {};

        for (var i = 0; i < selectedRadios.length; i++) {
            outputData[selectedRadios[i].name] = selectedRadios[i].value;
        }

        var outputContainer = document.getElementById('send-output');
        outputContainer.innerHTML = JSON.stringify(outputData, null, 4);
        outputContainer.style.display = 'block';
    }
}

function wyczyscFormularz() {
    usunAlerty();
    wyczyscElement('step-2', true);
    wyczyscElement('step-3', true);
    wyczyscElement('step-1');

    $('#step-1-nip').addClass('hidden');
    $('#step-1-pesel').addClass('hidden');

    ukryjWyslaneDane();

    zaladujDane();
}

function wyczyscElement(elementId, showHelper) {
    document.getElementById(elementId).innerHTML = '';
    if (showHelper) {
        document.getElementById(elementId).parentNode.querySelector('h5').style.display = 'block';
    }
}

function walidacja() {
    usunAlerty();

    var isValid = true;
    var checkedInputs = document.querySelectorAll('input[type="radio"]:checked');
    var errorText;

    if (checkedInputs.length === 0) {
        isValid = false;
        errorText = 'Proszę wybrać typ klienta.';
    } else if (checkedInputs.length === 1) {
        isValid = false;
        errorText = 'Proszę wybrać produkt.';
    } else if (checkedInputs.length === 2) {
        isValid = false;
        errorText = 'Proszę wybrać okres.';
    }

    // sprawdź czy NIP lub PESEL jest wypełniony
    var typKlienta = document.querySelector('input[name="klient"]:checked');

    if (typKlienta.getAttribute('value') === 'ngo' || typKlienta.getAttribute('value') === 'firma') {
        var nip = $('#step-1-nip input').val();
        if (nip === ''){
            isValid = false;
            errorText = 'Proszę podać NIP';
        }
    }

    if (!isValid) {
        utworzAlert(errorText);
    }

    return isValid;
}

function utworzAlert(myError) {
    var eContainer = document.createElement('div');
    eContainer.setAttribute('class', 'alert alert-danger');
    eContainer.innerHTML = myError;

    document.getElementById('send-output').parentNode.appendChild(eContainer);
}

function usunAlerty() {
    // $('.alert.alert-danger').remove();
    var alerts = document.getElementsByClassName('alert');

    for (var i = 0; i < alerts.length; i++) {
        alerts[i].parentNode.removeChild(alerts[i]);
    }
}

function ukryjHelper(elementId) {
    document.getElementById(elementId).parentNode.querySelector('h5').style.display = 'none';
}

function ukryjWyslaneDane() {
    var output = document.getElementById('send-output');
    if (output.style.display === 'block') {
        output.style.display = 'none';
    }
}