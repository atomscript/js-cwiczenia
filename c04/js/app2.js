const typFirma = 1;
const typIndywidualny = 2;

var jsonData;

$.getJSON('http://atomscript.com/pl/json/index.php', function (json) {
    jsonData = json;
});

function pokazProdukty(typKlienta) {
    clearElement('step-2');

    var myData = getJsonData(typKlienta);

    for (var i = 0; i < myData.length; i++) {
        var produkt = myData[i];
        var oprocentowanie = produkt.oprocentowanie * 100;
        var etykietaText = produkt.produkt + ' (' + oprocentowanie + '% w skali roku)';
        var additionalAttributes = [
            ['data-id', i], 
            ['data-parent', typKlienta]
        ];

        pContainer = generateRadioInput('produkt', produkt.produkt, etykietaText, additionalAttributes);

        // pInput.onchange = function () { pokazOkresy(this) };
        pContainer.querySelector('input').onchange = function () { pokazOkresy(this) };

        document.getElementById('step-2').appendChild(pContainer);
    }

    document.getElementById('step-2').parentNode.querySelector('h5').style.display = 'none';
}

function pokazOkresy(produkt) {
    clearElement('step-3');
    var produktId = produkt.getAttribute('data-id');
    var typKlienta = produkt.getAttribute('data-parent');

    var myData = getJsonData(typKlienta)[produktId].okresy;

    for (var i = 0; i < myData.length; i++) {
        var okresInput = generateRadioInput('okres', myData[i], myData[i]+' miesięcy');
        
        document.getElementById('step-3').appendChild(okresInput);
    }
}

function clearElement(elementId) {
    document.getElementById(elementId).innerHTML = '';
}

function getJsonData(typKlienta) {
    var myData = jsonData;

    typKlienta = parseInt(typKlienta);

    if (typKlienta === typFirma) {
        myData = jsonData.firma;
    } else if (typKlienta === typIndywidualny) {
        myData = jsonData.indywidualny;
    }

    return myData;
}

function generateRadioInput(inputName, inputValue, inputLabel, additionalAttributes) {

    var pContainer = document.createElement('div');
    pContainer.setAttribute('class', 'radio');

    var etykieta = document.createTextNode(inputLabel);
    var pInput = document.createElement('input');
    pInput.setAttribute('type', 'radio');
    pInput.setAttribute('name', inputName);
    pInput.setAttribute('value', inputValue);

    if (typeof additionalAttributes !== 'undefined') {
        for (var i = 0; i < additionalAttributes.length; i++) {
            pInput.setAttribute(additionalAttributes[i][0], additionalAttributes[i][1]);
        }
    }

    pInputContainer = document.createElement('label');
    pInputContainer.appendChild(pInput);
    pInputContainer.appendChild(etykieta);

    pContainer.appendChild(pInputContainer);

    return pContainer;
}