(function () {
    var typKlienta = document.getElementsByName('typKlienta');

    for (var i = 0; i < typKlienta.length; i++) {
        typKlienta[i].addEventListener('change', function () {
            if (this.getAttribute('id') === 'typ-firma') {
                document.getElementById('nip').classList.remove('hidden');
                document.getElementById('pesel').classList.add('hidden');
            } else {
                document.getElementById('pesel').classList.remove('hidden');
                document.getElementById('nip').classList.add('hidden');
            }
        });
    }
})();


function wyslijDane() {
    $('.alert').remove();

    var numberInputs = document.querySelectorAll('input[type="number"]');
    var textInputs = document.querySelectorAll('input[type="text"]');
    var radioInput = document.querySelector('input[type="radio"]:checked');
    var checkboxInputs = document.querySelectorAll('input[type="checkbox"]:checked');

    for (var i = 0; i < numberInputs.length; i++) {
        var elementName = numberInputs[i].getAttribute('name');
        if (elementName === 'kredyt') {
            if (parseFloat(numberInputs[i].value) === 0 || numberInputs[i].value.length === 0) {
                utworzAlert('Proszę podać kwotę kredytu!');
            }
        } else if (elementName === 'pesel') {
            if (radioInput !== null && radioInput.value === 'osoba' && numberInputs[i].value.length !== 11) {
                utworzAlert('Proszę podać prawidłowy PESEL!');
            }
        } else if (elementName === 'nip') {
            if (radioInput !== null && radioInput.value === 'firma' && numberInputs[i].value.length !== 10) {
                utworzAlert('Proszę podać prawidłowy NIP! np. 7831234567');
            }
        }
    }

    if (checkboxInputs.length === 0) {
        utworzAlert('Proszę wybrać okres kredytowania!');
    }

    if (radioInput === null) {
        utworzAlert('Proszę wybrać typ klienta!');
    }

    if (textInputs[0].value.length === 0) {
        utworzAlert('Proszę podać adres email!');
    }

    if ($('.alert').length === 0) {
        utworzAlert('Dane wypełnione poprawnie', 'success');
    }
}

function utworzAlert(myError, type) {
    if (typeof type === 'undefined') {
        type = 'danger';
    }
    var eContainer = document.createElement('div');
    eContainer.setAttribute('class', 'alert alert-' + type);
    eContainer.innerHTML = myError;

    document.getElementById('send-output').parentNode.appendChild(eContainer);
}